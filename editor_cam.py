from helper_functions import *
import pygame

class Camera:
    def __init__(self, size):
        self.width = size[0]
        self.height = size[1]
        self.x = 0
        self.y = 0
        self.scroll_buffer = 50
        self.scroll_speed = 8
    
    def update(self):
        #Moves the camera if mouse cursor is near edge of screen.
        mouse_pos = pygame.mouse.get_pos()
        dirx, diry = 0, 0
        if mouse_pos[0] <= self.scroll_buffer: dirx = -1
        elif mouse_pos[0] >= self.width-self.scroll_buffer: dirx = 1
        if mouse_pos[1] <= self.scroll_buffer: diry = -1
        elif mouse_pos[1] >= self.height-self.scroll_buffer: diry = 1

        self.x += dirx * self.scroll_speed
        self.y += diry * self.scroll_speed
        #Camera is clamped to never leave the bounds of the map-- Even if the target is in a corner.
        self.x = clamp(self.x, -100, 5000)
        self.y = clamp(self.y, -100, 600)  