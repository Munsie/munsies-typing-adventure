import os, sys
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide" #Disable pygame version/greeting message in console.
from helper_functions import *
import type_game, type_results, type_endless
from type_rooms import *
import json
print("Loading pygame...")
import pygame

print("Initializing pygame...")
pygame.init()

def load_level_data(font, difficulty):
    with open('data.json', 'r', encoding='utf-8') as f:
        data = json.load(f)
    rooms = []
    for key in data:
        new_room = Room(data[key]['id'], data[key]['name'])
        for obj in data[key]['objects']:
            new_obj = Object(obj['file'], obj['x'], obj['y'])
            new_room.objects.append(new_obj)
        for sp in data[key]['spawns']:
            if sp['file'] == "enemy1.png":
                en = Enemy(sp['x'], choose_word(difficulty), font)
                new_room.enemies.append(en)
            elif sp['file'] == "exit.png":
                new_room.exit = {'x': sp['x'], 'next_room': sp['value']}
            elif sp['file'] == "door.png":
                en = LockPanel(sp['x'], generate_captcha(difficulty), font)
                new_room.enemies.append(en)
                new_room.boss_exit = LockDoor(sp['x'], sp['value'])
            elif sp['file'] == "med.png":
                med = Medkit(sp['x'], choose_word(difficulty), font)
                new_room.enemies.append(med)
            elif sp['file'] == "zboss1.png":
                en = BigBoss(sp['x'], difficulty, font)
                new_room.enemies.append(en)
            elif sp['file'] == "zboss2.png":
                en = GlitchBoss(sp['x'], difficulty, font)
                new_room.enemies.append(en)
            elif sp['file'] == "zboss3.png":
                en = ReplicateBoss(sp['x'], difficulty, font)
                new_room.enemies.append(en)
        if difficulty == "easy": #Remove every other enemy, switch to del [2::3] to remove every third enemy if it's too easy.
            new_room.enemies = new_room.enemies[::2]
        rooms.append(new_room)
    
    rooms.append(Room(12, "Results")) #Dummy room to trigger the 'you win' state.
    return rooms

def type_option(screen, font, typed, word, x, y):
    if word.startswith(typed):
        untyped = word[len(typed):]
        typed_surf = font.render(typed, True, (0,183,0))
        typed_size = font.size(typed) 
        untyped_surf = font.render(untyped, True, (255,255,255))
        screen.blit(typed_surf, (x,y))
        screen.blit(untyped_surf, (x+typed_size[0], y))
    else:
        screen.blit( font.render(word, True, (255,255,255)) , (x,y))

def main():
    os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (150,75) #Defines the position of the top-left corner of the window when it opens.
    
    clock = pygame.time.Clock()
    SCREEN_SIZE = (1600,560)
    screen = pygame.display.set_mode(SCREEN_SIZE, pygame.SRCALPHA)
    pygame.display.set_caption("Munsie's Typing Adventure")

    title_font = pygame.font.Font(os.path.join('Fonts', 'Ubuntu-Regular.ttf'), 72)
    main_font = pygame.font.Font(os.path.join('Fonts', 'Ubuntu-Regular.ttf'), 32)
    small_font = pygame.font.Font(os.path.join('Fonts', 'Ubuntu-Regular.ttf'), 26)
    story_font = pygame.font.Font(os.path.join('Fonts', 'Ubuntu-Regular.ttf'), 22)
    
    bg = pygame.image.load(os.path.join('Sprites', 'Chunk1.png')) #One off sprite for the menu's background.
    size = bg.get_size()
    bg = pygame.transform.scale(bg, (size[0]*4, size[1]*4))

    typed_letters = ""
    difficulties = ["easy", "normal", "difficult", "infinite"]
    
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT: sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE: sys.exit()
                if event.key == pygame.K_BACKSPACE:
                    if len(typed_letters) >= 1: #Backspace only active if letters are actually inputted.
                        typed_letters = typed_letters[:-1]
                if event.unicode in [difficulties[0][0], difficulties[1][0], difficulties[2][0]] and typed_letters == "":
                    typed_letters += event.unicode
                else:
                    index = len(typed_letters)
                    for diff in difficulties: #Check if letter typed is next letter in difficulty name.
                        if index < len(diff):
                            next_letter = diff[index]
                            if diff.startswith(typed_letters) and event.unicode == next_letter:
                                typed_letters += event.unicode

        #Draw stuff
        screen.fill((0,0,0))
        screen.blit(bg, (-5,0)) #Render the background sprite.
        
        screen.blit( title_font.render("Munsie's Typing Adventure", True, (255,255,255)) , (60,110))
        
        screen.blit( small_font.render("Type the difficulty of your choice to begin:", True, (255,255,255)) , (150,250))
        type_option(screen, main_font, typed_letters, difficulties[0], 200, 300)
        type_option(screen, main_font, typed_letters, difficulties[1], 200, 340)
        type_option(screen, main_font, typed_letters, difficulties[2], 200, 380)
        type_option(screen, main_font, typed_letters, difficulties[3], 200, 440)
        
        screen.blit( story_font.render("After several years in cryo sleep you awaken to find the ship's", True, (255,255,255)) , (915, 385))
        screen.blit( story_font.render("combat and worker drones have turned against the crew. Stop as", True, (255,255,255)) , (915, 420))
        screen.blit( story_font.render("many bots as you can while fighting your way to the escape pods!", True, (255,255,255)), (915, 455))
        
        pygame.display.flip()
        
        for diff in difficulties:
            if typed_letters == diff: #If player has typed the full diff name, start up main game script.
                if diff == "infinite": #Endless wave defense mode is treated differently.
                    stats = type_endless.play(screen, clock, main_font)
                    if stats.total_keys > 0 and stats.damage_taken > 0:
                        type_results.results(screen, clock, bg, stats, main_font, title_font)
                    typed_letters = "" #Resets typed letters to avoid game auto-restarting when they return to menu.
                else:
                    print("Loading level data...")
                    rooms = load_level_data(main_font, diff)
                    stats = type_game.play(SCREEN_SIZE, screen, clock, diff, main_font, rooms)
                    if stats.total_keys > 0 and stats.damage_taken > 0:
                        type_results.results(screen, clock, bg, stats, main_font, title_font)
                    typed_letters = "" #Resets typed letters to avoid game auto-restarting when they return to menu.
        
        clock.tick(144)
    
main()