import os, sys, random
from helper_functions import *
import json
from type_rooms import *

class Player:
    def __init__(self):
        self.sprite = load_sprite('Character 1.png')
        self.sprite = pygame.transform.flip(self.sprite, True, False)
        self.width = self.sprite.get_width()
        self.x = 50
        self.y = 197
        self.speed = 0.15
        self.state = "WALK"
        self.max_life = 5
        self.life = self.max_life
    
    def update(self, delta):
        if self.state == "WALK":
            self.x += self.speed * delta
        elif self.state == "WAIT":
            pass
    
    def draw(self, screen, camera):
        screen.blit(self.sprite, (round(self.x)-camera.x, round(self.y)-camera.y))
        
class Camera:
    def __init__(self, target):
        self.target = target
        self.offset = 150
        self.x = round(target.x) - self.offset
        self.y = 0
        
    def update(self):
        self.x = round(self.target.x) - self.offset
        
class Fade:
    def __init__(self, size):
        self.x = 0
        self.y = 0
        self.width = size[0]
        self.height = size[1]
        self.surf = pygame.Surface(size, pygame.SRCALPHA)
        self.state = "IDLE"
        self.alpha = 0
        self.next_room = None
        
    def update(self, delta):
        fade_value = delta/3
        if self.state == "OUT": #If fading out, increase alpha value until blackness is fully visible.
            self.alpha += fade_value
            if self.alpha >= 255:
                self.state = "IN"
                pygame.event.post(pygame.event.Event(pygame.USEREVENT+2, next_room=self.next_room)) #Switch rooms when full darkness is achieved.
        elif self.state == "IN": #If fading in, decrease alpha value until blackness disappears.
            self.alpha -= fade_value
            if self.alpha <= 0:
                self.state = "IDLE"
                pygame.event.post(pygame.event.Event(pygame.USEREVENT+3)) #Player starts moving again when darkness is gone.
                
        self.alpha = clamp(self.alpha, 0, 255)
        
        return False
        
    def draw(self, screen):
        self.surf.fill((0,0,0,self.alpha))
        screen.blit(self.surf, (self.x, self.y))

class Stats:
    def __init__(self, difficulty):
        self.difficulty = difficulty
        self.total_keys = 0
        self.correct_keys = 0
        self.enemies_killed = 0
        self.bosses_killed = 0
        self.damage_taken = 0
        self.healing = 0
        self.dead = False
        self.escaped = False
        self.time_elapsed = 0
        
def load_sprite(file): #Custom loader that scales sprite to 4x size before returning.
    image = pygame.image.load(os.path.join('Sprites', file))
    size = image.get_size()
    return pygame.transform.scale(image, (size[0]*4, size[1]*4))

def play(SCREEN_SIZE, screen, clock, difficulty, font, rooms):
    #Custom events, mostly triggered by other objects (enemies, doors, etc).
    DEATH_EVENT = pygame.USEREVENT
    FADE_OUT_EVENT = pygame.USEREVENT+1
    FADE_IN_EVENT = pygame.USEREVENT+2
    FADE_COMPLETE_EVENT = pygame.USEREVENT+3
    UNLOCK_DOOR = pygame.USEREVENT+4
    HEAL_OR_DAMAGE = pygame.USEREVENT+5
    NEW_ENEMY = pygame.USEREVENT+6
    BOSS_DEATH_EVENT = pygame.USEREVENT+7

    delta_time = 0

    player = Player()
    camera = Camera(player)
    fade = Fade(SCREEN_SIZE)
    stats = Stats(difficulty)
    
    current_room = rooms[0]
    typed_letters = ""
    targets = []
    playing_game = True
    
    while playing_game:
        for event in pygame.event.get():
            if event.type == pygame.QUIT: sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE: return(stats)
                if event.key == pygame.K_BACKSPACE:
                    typed_letters = ""
                    targets = []
                else:
                    targets = []
                    stats.total_keys += 1
                    typed_letters += event.unicode #Add the typed letter to the currently inputted string
                    for enemy in current_room.enemies:
                        if enemy.word.word.startswith(typed_letters) and abs(player.x - enemy.x) <= 1440: #If the inputted string matches the enemy's word so far, add them to targets
                            targets.append(enemy)
                            
                    if len(targets) >= 1:
                        stats.correct_keys += 1
                    else:
                        typed_letters = typed_letters[:-1] #If there's no valid targets, remove the most recent letter (as it's incorrect).
                        for enemy in current_room.enemies:
                            if enemy.word.word.startswith(typed_letters): #Rebuild target list.
                                targets.append(enemy)

            if event.type == DEATH_EVENT:
                if event.enemy.word.word.startswith(typed_letters):
                    typed_letters = ""
                    targets = []
                index = current_room.enemies.index(event.enemy)
                del current_room.enemies[index]
                stats.enemies_killed += 1
            if event.type == BOSS_DEATH_EVENT:
                if event.words > 0:
                    typed_letters = ""
                    targets = []
                else:
                    if event.enemy.word.word.startswith(typed_letters):
                        typed_letters = ""
                        targets = []
                    index = current_room.enemies.index(event.enemy)
                    del current_room.enemies[index]
                    stats.bosses_killed += 1
                    player.state = "WALK"
            if event.type == FADE_OUT_EVENT: #Begin fading out, sets the next room to transition to.
                fade.state = "OUT"
                player.state = "WAIT"
                fade.next_room = event.next_room
            if event.type == FADE_IN_EVENT: #Begin fading in and switch to the next room.
                fade.state = "IN"
                current_room = rooms[event.next_room]
                typed_letters = ""
                player.x = 100
            if event.type == FADE_COMPLETE_EVENT: #Fade is complete, make the player start moving again.
                player.state = "WALK"
            if event.type == UNLOCK_DOOR:
                current_room.boss_exit.unlock()
            if event.type == HEAL_OR_DAMAGE:
                if event.amount >= 0: stats.damage_taken += event.amount
                if event.amount < 0: stats.healing += event.amount
                player.life -= event.amount
                if player.life > 5:
                    player.life = 5
            if event.type == NEW_ENEMY:
                current_room.enemies.append(event.enemy)
        
        player.update(delta_time)
        if current_room.exit != None:
            if abs(player.x - current_room.exit['x']) <= 20 and fade.state == "IDLE": #Check if the player has reached the exit.
                pygame.event.post(pygame.event.Event(FADE_OUT_EVENT, next_room=current_room.exit['next_room']))
        if current_room.boss_exit != None: #Check if the player has reached the boss exit.
            if player.x >= current_room.boss_exit.x + current_room.boss_exit.door_offset and fade.state == "IDLE" and current_room.boss_exit.state == "UNLOCKED":
                pygame.event.post(pygame.event.Event(FADE_OUT_EVENT, next_room=current_room.boss_exit.next_room))
        for enemy in current_room.enemies:
            enemy.update(delta_time, player)
        for target in targets: #Pass the currently typed string along to each valid target so they can display properly.
            target.word.typed = typed_letters
            if target.word.typed == target.word.word: #If the word is fully typed, kill off the enemy.
                target.die()
        camera.update()
        fade.update(delta_time)
        stats.time_elapsed += delta_time
        
        #Check for win and loss states
        if player.life <= 0:
            stats.dead = True
            return(stats)
        if current_room.name == "Results":
            stats.escaped = True
            return(stats)
        
        #Draw objects
        screen.fill((0,0,0))
        for obj in current_room.objects:
            screen.blit(obj.sprite, (obj.x-camera.x, obj.y-camera.y))
        if current_room.boss_exit != None:
            current_room.boss_exit.draw(screen, camera)
        for enemy in current_room.enemies:
            enemy.draw(screen, camera)
        player.draw(screen, camera)
        fade.draw(screen)
        
        #Draw UI
        screen.blit( font.render(f"Life: {player.life}", True, (255,255,255)) , (5,525))
        if stats.total_keys == 0:
            pct = "100.00%"
        else:
            pct = "{0:.2f}".format((stats.correct_keys/stats.total_keys)*100) + "%"
        screen.blit( font.render(pct, True, (255,255,255)) , (700,525))
        xpos = SCREEN_SIZE[0] - font.size(current_room.name)[0] - 10
        screen.blit( font.render(current_room.name, True, (255,255,255)), (xpos,525))
        
        pygame.display.flip()
        
        delta_time = clock.tick(144)