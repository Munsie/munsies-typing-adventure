import pygame
from type_game import Stats
from type_rooms import Word
import random, os

class Enemy:
    def __init__(self, sprite, x, word, font):
        self.sprite = sprite
        self.height = self.sprite.get_height()
        self.width = self.sprite.get_width()
        self.x = x
        self.y = 480 - self.height
        self.word = Word(self, word, font)
        self.speed = 0.1
        self.damage = 0.5
        self.death_event = pygame.event.Event(pygame.USEREVENT, enemy=self)
    
    def update(self, delta):
        self.word.typed = "" #Clear the word progress, updated progress is passed in immediately after update() call.
        self.x -= self.speed * delta
        if abs(self.x - 755) <= 60:
            pygame.event.post(pygame.event.Event(pygame.USEREVENT+5, amount=self.damage))
            self.die()
        
    def die(self):
        pygame.event.post(self.death_event)
    
    def draw(self, screen, camera):
        screen.blit(self.sprite, (round(self.x), round(self.y)))
        self.word.draw(screen, camera)

class Director():
    def __init__(self, font):
        self.font = font
        self.min_length = 3
        self.max_length = 5
        self.spawn_direction = 1
        self.action_threshold = 2200
        self.action_time = 0
        self.action_count = 0
        enemy2 = load_sprite('Character 2.png', 1)
        enemy3 = load_sprite('Character 3.png', 1)
        enemy4 = load_sprite('Character 4 Blue.png', 1)
        self.enemy_sprites = [scale_sprite(enemy2, 4), scale_sprite(enemy3, 4), scale_sprite(enemy4, 4),
                              scale_sprite(enemy2, 3), scale_sprite(enemy3, 3), scale_sprite(enemy4, 3),
                              scale_sprite(enemy2, 2), scale_sprite(enemy3, 2), scale_sprite(enemy4, 2)]
        
    def update(self, delta):
        self.action_time += delta
        if self.action_time >= self.action_threshold:
            self.action_count += 1
            if self.action_count % 4 == 0:
                self.max_length += 1
            elif self.action_count % 8 == 0:
                self.min_length += 1
                self.max_length += 1
            if self.min_length > 8: self.min_length = 8
            if self.max_length > 16: self.max_length = 16
            self.action_time -= self.action_threshold
            self.action_threshold -= 10 #Speed up spawns after each one.
            if self.action_threshold < 800: self.action_threshold = 800
            
            self.spawn_enemy()
            self.spawn_direction = self.spawn_direction * -1 #Alternate which side enemies spawn on.
            
    def choose_word(self):        
        with open('wordlist.txt') as wordbook:
            words = [line for line in wordbook if self.max_length >= len(line) >= self.min_length]
        return random.choice(words).rstrip('\n')
            
    def spawn_enemy(self):
        x = 800 + (950 * self.spawn_direction)
        new_enemy = Enemy(random.choice(self.enemy_sprites), x, self.choose_word(), self.font)
        if self.spawn_direction < 0:
            new_enemy.sprite = pygame.transform.flip(new_enemy.sprite, True, False) #Flip enemy sprite to match which direction they're coming from.
            new_enemy.speed = new_enemy.speed * -1 #Reverse their speed as well so they move towards the middle.
        pygame.event.post(pygame.event.Event(pygame.USEREVENT+6, enemy=new_enemy))

class Camera:
    def __init__(self):
        self.x = 0
        self.y = 0

def load_sprite(file, scale): #Custom loader that scales sprite to custom size multiplier before returning.
    image = pygame.image.load(os.path.join('Sprites', file))
    size = image.get_size()
    return pygame.transform.scale(image, (size[0]*scale, size[1]*scale))
    
def scale_sprite(image, scale):
    size = image.get_size()
    return pygame.transform.scale(image, (size[0]*scale, size[1]*scale))

def play(screen, clock, font):
    delta_time = 0
    stats = Stats("infinite")
    
    camera = Camera() #Dummy camera that literally doesn't do anything because the Word class expects one.
    bg = load_sprite('Chunk1.png', 4)
    player = load_sprite('Character 1.png', 4)
    player_life = 5
    
    director = Director(font)
    enemy_list = []
    
    typed_letters = ""
    targets = []
    
    DEATH_EVENT = pygame.USEREVENT
    DAMAGE_EVENT = pygame.USEREVENT+5
    NEW_ENEMY = pygame.USEREVENT+6
    
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT: sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE: return(stats)
                if event.key == pygame.K_BACKSPACE:
                    typed_letters = ""
                    targets = []
                else:
                    targets = []
                    stats.total_keys += 1
                    typed_letters += event.unicode #Add the typed letter to the currently inputted string
                    for enemy in enemy_list:
                        if enemy.word.word.startswith(typed_letters): #If the inputted string matches the enemy's word so far, add them to targets
                            targets.append(enemy)
                            
                    if len(targets) >= 1:
                        stats.correct_keys += 1
                    else:
                        typed_letters = typed_letters[:-1] #If there's no valid targets, remove the most recent letter (as it's incorrect).
                        for enemy in enemy_list:
                            if enemy.word.word.startswith(typed_letters): #Rebuild target list.
                                targets.append(enemy)
            if event.type == DEATH_EVENT:
                    if event.enemy.word.word.startswith(typed_letters):
                        typed_letters = ""
                        targets = []
                    index = enemy_list.index(event.enemy)
                    del enemy_list[index]
                    stats.enemies_killed += 1
            if event.type == NEW_ENEMY:
                enemy_list.append(event.enemy)
            if event.type == DAMAGE_EVENT:
                stats.damage_taken += event.amount
                player_life -= event.amount
        
        #Check for loss state
        if player_life <= 0:
            stats.dead = True
            return(stats)
        
        #Update
        stats.time_elapsed += delta_time
        director.update(delta_time)
        for enemy in enemy_list:
            enemy.update(delta_time)
        for target in targets: #Pass the currently typed string along to each valid target so they can display properly.
            target.word.typed = typed_letters
            if target.word.typed == target.word.word: #If the word is fully typed, kill off the enemy.
                target.die()
        
        #Draw
        screen.fill((0,0,0))
        screen.blit(bg, (-400, -13))
        screen.blit(player, (800-(player.get_width()/2), 197))
        for enemy in enemy_list:
            enemy.draw(screen, camera)
            
        #Draw UI
        screen.blit( font.render(f"Life: {player_life}", True, (255,255,255)) , (5,525))
        if stats.total_keys == 0:
            pct = "100.00%"
        else:
            pct = "{0:.2f}".format((stats.correct_keys/stats.total_keys)*100) + "%"
        screen.blit( font.render(pct, True, (255,255,255)) , (700,525))
        xpos = 1590 - font.size("Infinite")[0]
        screen.blit( font.render("Infinite", True, (255,255,255)), (xpos,525))
            
        pygame.display.flip()
        
        delta_time = clock.tick(144)