import os, sys, random
from helper_functions import *
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide" #Disable pygame version/greeting message in console.
print("Loading pygame...")
import pygame
from editor_cam import Camera
import json

print("Initializing pygame...")
pygame.init()
type_font = pygame.font.SysFont('Arial.ttf', 40)

class Room:
    def __init__(self, id, name):
        self.name = name
        self.id = id
        self.objects = []
        self.spawns = []

class Object:
    def __init__(self, pos, img):
        self.x = pos[0]
        self.y = pos[1]
        self.file = img[0]
        self.sprite = img[1]
        self.value = None

def import_json(image_list, spawn_list):
    with open('data.json', 'r', encoding='utf-8') as f:
        data = json.load(f)
    
    room_list = []
    
    for key in data:
        new_room = Room(data[key]['id'], data[key]['name'])
        for obj in data[key]['objects']:
            file = obj['file']
            for image in image_list:
                if file in image:
                    sprite = image[1]
            img = (file, sprite)
            pos = (obj['x'], obj['y'])
            new_obj = Object(pos, img)
            new_room.objects.append(new_obj)
        for obj in data[key]['spawns']:
            file = obj['file']
            for image in spawn_list:
                if file in image:
                    sprite = image[1]
            img = (file, sprite)
            pos = (obj['x'], obj['y'])
            new_obj = Object(pos, img)
            new_obj.value = obj['value']
            new_room.spawns.append(new_obj)
        room_list.append(new_room)
    
    return room_list

def export_json(rooms):
    data = {}
    room_id = 0
    for room in rooms:
        data[room_id] = {}
        data[room_id]["id"] = room_id
        data[room_id]["name"] = room.name
        data[room_id]["objects"] = []
        for object in room.objects:
            obj = {}
            obj["file"] = object.file
            obj["x"] = object.x
            obj["y"] = object.y
            data[room_id]["objects"].append(obj)
        data[room_id]["spawns"] = []
        for spawn in room.spawns:
            sp = {}
            sp["file"] = spawn.file
            sp["x"] = spawn.x
            sp["y"] = spawn.y
            sp["value"] = spawn.value
            data[room_id]["spawns"].append(sp)
        room_id += 1
            
    with open('data.json', 'w', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False, indent=4)
    print("Data saved to JSON file.")

def main():
    if getattr(sys, 'frozen', False):
        os.chdir(sys._MEIPASS)

    os.environ['SDL_VIDEO_WINDOW_POS'] = "%d,%d" % (150,75) #Defines the position of the top-left corner of the window when it opens.
    
    clock = pygame.time.Clock()
    delta_time = 0
    
    SCREEN_SIZE = (1600,900)
    screen = pygame.display.set_mode(SCREEN_SIZE, pygame.SRCALPHA)
    pygame.display.set_caption("Typing")
    
    image_list = []
    for file in os.listdir('Sprites'):
        img = pygame.image.load(os.path.join('Sprites', file))
        size = img.get_size()
        big_img = pygame.transform.scale(img, (size[0]*4, size[1]*4))
        image_list.append((file, big_img))
    image_selection = 0
    
    spawn_list = []
    for file in os.listdir('Spawns'):
        img = pygame.image.load(os.path.join('Spawns', file))
        spawn_list.append((file, img))
    spawn_selection = 0
    
    spawn_layer = False
    spawn_visible = True
    grid_snap = 4
    
    room_list = []
    for x in range(1,13):
        room_list.append(Room(x, "Blah"))
    current_room = room_list[0]
    
    cam = Camera(SCREEN_SIZE)
    
    while True:
        mx, my = pygame.mouse.get_pos()
        mx = grid_snap * round(mx/grid_snap)
        my = grid_snap * round(my/grid_snap)
    
        for event in pygame.event.get():
            if event.type == pygame.QUIT: sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE: sys.exit()
                if 293 >= event.key >= 282: #f1-f12
                    current_room = room_list[event.key-282]
                    print(f"Switched to room: {current_room.id} - {current_room.name}")
                if event.key == 96: #tilde, list room's objects
                    index = 0
                    for obj in current_room.objects:
                        print(f"{index}. X: {obj.x}, Y: {obj.y}, Name: {obj.file}")
                        index += 1
                    print("-------")
                    index = 0
                    for spawn in current_room.spawns:
                        print(f"{index}. X: {spawn.x}, Y: {spawn.y}, Name: {spawn.file}, Value: {spawn.value}")
                        index += 1
                if event.key == 8: #backspace, remove object
                    try:
                        index = int(input("Enter the index of the object to remove: "))
                        current_room.objects.remove(current_room.objects[index])
                    except:
                        print("error removing object")
                if event.key == 127: #delete, remove spawn
                    try: 
                        index = int(input("Enter the index of the spawn to remove: "))
                        current_room.spawns.remove(current_room.spawns[index])
                    except:
                        print("error removing spawn")
                if event.key == 13: #enter, move object
                    index = int(input("Enter the index of the object to move: "))
                    obj = current_room.objects[index]
                    print(f"Moving object {obj.file}, currently at position ({obj.x}, {obj.y})")
                    obj.x = int(input("Enter new x: "))
                    obj.y = int(input("Enter new y: "))
                if event.key == 115: #S, save data to json
                    confirm = input("confirm you'd like to save existing levels over data.json? (y/n)")
                    if confirm == "y":
                        export_json(room_list)
                    else:
                        print("data save aborted")
                if event.key == 108: #L, load data from json
                    confirm = input("confirm you'd like to load data.json over existing levels? (y/n)")
                    if confirm == "y":
                        room_list = import_json(image_list, spawn_list)
                        current_room = room_list[0]
                    else:
                        print("data load aborted")
                if event.key == 110: #N, rename
                    current_room.name = input(f"Enter a new name for room {current_room.id}, current name {current_room.name}: ")
                if event.key == 45: #-, toggle mode
                    spawn_layer = not spawn_layer
                if event.key == 61: #+, toggle spawn visibility
                    spawn_visible = not spawn_visible
                if event.key == 118: #V, add value to spawn
                    index = int(input("Enter the index of the spawn to add a value to: "))
                    current_room.spawns[index].value = int(input("Enter the value: "))
            if event.type == pygame.MOUSEBUTTONDOWN:
                if not spawn_layer:
                    if event.button == 4:
                        image_selection = clamp(image_selection-1, 0, len(image_list)-1)
                    elif event.button == 5:
                        image_selection = clamp(image_selection+1, 0, len(image_list)-1)
                    elif event.button == 1:
                        new_obj = Object((mx+cam.x, my+cam.y), image_list[image_selection])
                        current_room.objects.append(new_obj)
                elif spawn_layer:
                    if event.button == 4:
                        spawn_selection = clamp(spawn_selection-1, 0, len(spawn_list)-1)
                    elif event.button == 5:
                        spawn_selection = clamp(spawn_selection+1, 0, len(spawn_list)-1)
                    elif event.button == 1:
                        new_obj = Object((mx+cam.x, my+cam.y), spawn_list[spawn_selection])
                        new_obj.value = None
                        current_room.spawns.append(new_obj)

        cam.update()
        
        screen.fill((255,255,255))
        pygame.draw.rect(screen, (0,0,0), (0-cam.x, 0-cam.y, 3000, 528), 1)
        for obj in current_room.objects:
            screen.blit(obj.sprite, (obj.x-cam.x, obj.y-cam.y))
        if spawn_visible:
            for sp in current_room.spawns:
                screen.blit(sp.sprite, (sp.x-cam.x, sp.y-cam.y))
        if spawn_layer: screen.blit(spawn_list[spawn_selection][1], (mx,my))
        else: screen.blit(image_list[image_selection][1], (mx,my))
        pygame.display.flip()
        
        delta_time = clock.tick(144)
        
if __name__ == "__main__":
    main()