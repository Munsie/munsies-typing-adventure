#Various helper and math functions

def clamp(value, minimum, maximum):
    return max(minimum, min(value, maximum))
    
def round_down(value, divisor):
    return value - (value % divisor)
    
def most_frequent(list):
    if len(list) == 0:
        return None
    
    value = list[0]
    count = 0
    for i in list:
        frequency = list.count(i)
        if(frequency > count):
            value = i
            count = frequency
    return value