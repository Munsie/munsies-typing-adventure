import os, pygame, random, string

class Room:
    def __init__(self, id, name):
        self.id = id
        self.name = name
        self.objects = []
        self.enemies = []
        self.exit = None
        self.boss_exit = None

class Object:
    def __init__(self, file, x, y):
        self.x = x
        self.y = y
        self.file = file
        self.sprite = load_sprite(file)

class Enemy:
    def __init__(self, x, word, font):
        self.sprite = load_sprite('Character 3.png')
        self.width = self.sprite.get_width()
        self.x = x
        self.y = 180
        self.word = Word(self, word, font)
        self.speed = 0.05
        self.damage = 0.5
        self.death_event = pygame.event.Event(pygame.USEREVENT, enemy=self)
    
    def update(self, delta, player):
        self.word.typed = "" #Clear the word progress, updated progress is passed in immediately after update() call.
        if abs(self.x - player.x) <= 1500: #Only move if the player is within 1500 pixels, stops end of level from being empty.
            self.x -= self.speed * delta
        if self.x <= player.x+(player.width*0.75):
            pygame.event.post(pygame.event.Event(pygame.USEREVENT+5, amount=self.damage))
            self.die()
        
    def die(self):
        pygame.event.post(self.death_event)
    
    def draw(self, screen, camera):
        screen.blit(self.sprite, (round(self.x)-camera.x, round(self.y)-camera.y))
        self.word.draw(screen, camera)

class Boss: #Core functionality shared between all 3 bosses.
    def __init__(self, x, difficulty, font):
        self.sprite = load_sprite('Character 2.png')
        self.objects = []
        self.width = self.sprite.get_width()
        self.x = x
        self.y = 180
        self.font = font
        self.difficulty = difficulty
        self.words = []
        self.action_time = 0
        self.action_list = ["pass"] #Default action_list, overridden by boss subclasses.

    def update(self, delta, player):
        self.word.typed = ""
        if abs(self.x - player.x) <= 1000 and player.state == "WALK":
            player.state = "WAIT"
        if player.state == "WAIT":
            self.action_time += delta
            if self.action_time >= self.action_threshold:
                self.action_time -= self.action_threshold
                new_action = self.action_list.pop(0)
                self.action_list.append(new_action)
                if new_action == "attack":
                    self.attack()
                elif new_action == "add word":
                    self.add_word()
                    
    def die(self):
        pygame.event.post(pygame.event.Event(pygame.USEREVENT+7, enemy=self, words=len(self.words)))
        if len(self.words) > 0:
            self.word = self.words.pop(0)
            
    def draw(self, screen, camera):
        screen.blit(self.sprite, (self.x-camera.x, self.y-camera.y))
        for obj in self.objects:
            screen.blit(obj.sprite, (obj.x-camera.x, obj.y-camera.y))
        self.word.draw(screen, camera)
        
        pips = len(self.words)
        pip_size = 7
        xoff = 4
        pip_width = (pips * pip_size) + ((pips-1) * xoff)
        xpos = (self.sprite.get_size()[0]/2) - (pip_width/2)
        for p in range(pips):
            pygame.draw.rect(screen,(255,255,255),(self.x+xpos-camera.x,120-camera.y,pip_size,pip_size))
            xpos += pip_size + xoff
    
class BigBoss(Boss):
    def __init__(self, x, difficulty, font):
        super().__init__(x, difficulty, font)

        #Tuning boss encounter based on difficulty level
        if difficulty == "easy":
            boss_words = 3
            self.word_diff = "normal"
            self.attack_diff = "easy"
            self.action_threshold = 2200
        elif difficulty == "normal":
            boss_words = 4
            self.word_diff = "difficult"
            self.attack_diff = "normal"
            self.action_threshold = 1600
        elif difficulty == "difficult":
            boss_words = 6
            self.word_diff = "uber"
            self.attack_diff = "difficult"
            self.action_threshold = 1200
            
        self.generate_words(boss_words)
        self.action_list = ["attack", "attack", "pass", "add word"]

    def generate_words(self, word_count):
        for i in range(word_count):
            new_word = Word(self, choose_word(self.word_diff), self.font)
            new_word.word = new_word.word.upper()
            new_word.calc_box()
            self.words.append(new_word)
        self.word = self.words.pop(0)
         
    def attack(self):
        bullet = Enemy(self.x - 50, choose_word(self.difficulty), self.font)
        bullet.sprite = load_sprite('Neon.png')
        bullet.y += 100
        bullet.speed = 0.20
        pygame.event.post(pygame.event.Event(pygame.USEREVENT+6, enemy=bullet))
        
    def add_word(self):
        new_word = Word(self, choose_word(self.word_diff), self.font)
        new_word.word = new_word.word.upper()
        new_word.calc_box()
        self.words.append(new_word)
        
class GlitchBoss(Boss):
    def __init__(self, x, difficulty, font):
        super().__init__(x, difficulty, font)
        self.objects.append( Object('Screen info 3.png', self.x+8, self.y+12) )

        #Tuning boss encounter based on difficulty level
        if difficulty == "easy":
            boss_words = 3
            self.word_diff = "easy"
            self.attack_diff = "easy"
            self.action_threshold = 2200
        elif difficulty == "normal":
            boss_words = 4
            self.word_diff = "easy"
            self.attack_diff = "easy"
            self.action_threshold = 1600
        elif difficulty == "difficult":
            boss_words = 6
            self.word_diff = "normal"
            self.attack_diff = "normal"
            self.action_threshold = 1200
        
        self.generate_words(boss_words)
        self.action_list = ["attack", "pass", "attack", "pass", "add word", "pass"]

    def generate_words(self, word_count):
        for i in range(word_count):
            new_word = Word(self, generate_captcha(self.word_diff), self.font)
            self.words.append(new_word)
        self.word = self.words.pop(0)
            
    def attack(self):
        bullet = Enemy(self.x - 50, choose_word(self.attack_diff), self.font)
        bullet.sprite = load_sprite('Neon.png')
        bullet.y += 100
        bullet.speed = 0.20
        pygame.event.post(pygame.event.Event(pygame.USEREVENT+6, enemy=bullet))
        
    def add_word(self):
        new_word = Word(self, generate_captcha(self.word_diff), self.font)
        self.words.append(new_word)
        
class ReplicateBoss(Boss):
    def __init__(self, x, difficulty, font):
        super().__init__(x, difficulty, font)
        self.sprite = load_sprite('Character 4 Blue.png')
        
        image = pygame.image.load(os.path.join('Sprites', 'Character 4 Blue.png'))
        size = image.get_size()
        self.minion_sprite = pygame.transform.scale(image, (size[0]*2, size[1]*2))

        #Tuning boss encounter based on difficulty level
        if difficulty == "easy":
            boss_words = 3
            self.word_diff = "normal"
            self.attack_diff = "easy"
            self.action_threshold = 2000
        elif difficulty == "normal":
            boss_words = 4
            self.word_diff = "difficult"
            self.attack_diff = "normal"
            self.action_threshold = 1500
        elif difficulty == "difficult":
            boss_words = 6
            self.word_diff = "uber"
            self.attack_diff = "difficult"
            self.action_threshold = 1000
        
        self.generate_words(boss_words)
        self.action_list = ["attack", "pass", "pass", "add word", "pass"]

    def generate_words(self, word_count):
        for i in range(word_count):
            new_word = Word(self, choose_word(self.word_diff), self.font)
            self.words.append(new_word)
        self.word = self.words.pop(0)
        self.action_threshold -= 33
            
    def attack(self):
        #Spawn a dude on the left and right side of the boss.
        minion = Enemy(self.x - 200, choose_word(self.attack_diff), self.font)
        minion.sprite = self.minion_sprite
        minion.speed = 0.15
        minion.y += 150
        pygame.event.post(pygame.event.Event(pygame.USEREVENT+6, enemy=minion))
        
        minion = Enemy(self.x + 225, choose_word(self.attack_diff), self.font)
        minion.sprite = self.minion_sprite
        minion.speed = 0.15
        minion.y += 150
        pygame.event.post(pygame.event.Event(pygame.USEREVENT+6, enemy=minion))

    def add_word(self):
        new_word = Word(self, choose_word(self.word_diff), self.font)
        self.words.append(new_word)

class Medkit():
    def __init__(self, x, word, font):
        self.sprite = load_sprite('Health Pack 1.png')
        self.width = self.sprite.get_width()
        self.x = x
        self.y = 150
        self.word = Word(self, "+"+word, font)
        self.speed = 0
        self.damage = -1
        self.death_event = pygame.event.Event(pygame.USEREVENT, enemy=self)

    def update(self, delta, player):
        self.word.typed = ""
        if self.x <= player.x - 150:
            self.die() 
            
    def die(self):
        if self.word.typed == self.word.word:
            pygame.event.post(pygame.event.Event(pygame.USEREVENT+5, amount=self.damage))
        pygame.event.post(self.death_event)
        
    def draw(self, screen, camera):
        screen.blit(self.sprite, (self.x-camera.x, self.y-camera.y))
        self.word.draw(screen, camera)
        
class LockPanel():
    def __init__(self, x, word, font):
        self.sprite = load_sprite('Screen info 3.png')
        self.width = self.sprite.get_width()
        self.x = x
        self.y = 150
        self.word = Word(self, word, font)
        self.death_event = pygame.event.Event(pygame.USEREVENT, enemy=self)
        self.unlock_event = pygame.event.Event(pygame.USEREVENT+4)
        
    def update(self, delta, player):
        self.word.typed = "" #Clear the word progress, updated progress is passed in immediately after update() call.
        if self.x <= player.x:
            self.die()
            
    def die(self):
        pygame.event.post(self.death_event)
        if self.word.typed == self.word.word:
            pygame.event.post(self.unlock_event)
        
    def draw(self, screen, camera):
        screen.blit(self.sprite, (round(self.x)-camera.x, round(self.y)-camera.y))
        self.word.draw(screen, camera)
        
class LockDoor():
    def __init__(self, x, next_room):
        self.sprites = [load_sprite('Screen info 2.png'), load_sprite('Screen info 1.png')]
        self.sprite = self.sprites[0]
        self.door_sprites = [load_sprite('Door 2.png'), load_sprite('Open Door.png')]
        self.door_sprite = self.door_sprites[0]
        self.x = x
        self.y = 150
        self.door_offset = 120
        self.next_room = next_room
        self.state = "LOCKED"
        
    def unlock(self):
        self.sprite = self.sprites[1]
        self.door_sprite = self.door_sprites[1]
        self.state = "UNLOCKED"
        
    def draw(self, screen, camera):
        screen.blit(self.sprite, (self.x-camera.x, self.y-camera.y))
        screen.blit(self.door_sprite, (self.x-camera.x + self.door_offset, self.y-camera.y-68))

class Word:
    def __init__(self, parent, word, font):
        self.parent = parent
        self.word = word
        self.typed = ""
        self.font = font
        self.calc_box()
        
    def calc_box(self):
        boxw, boxh = self.font.size(self.word)
        box_size = (boxw + 10, boxh + 10)
        self.box = pygame.Surface(box_size, pygame.SRCALPHA)
        self.box.fill((255,255,255,175))
        
    def draw(self, screen, camera):
        untyped = self.word[len(self.typed):] #Determines the untyped portion of the word
        
        #Calculate how much space the typed and untyped portions of the word will take up.
        typed_rect = self.font.size(self.typed) 
        untyped_rect = self.font.size(untyped)
        
        #Calc the offset required to properly center the text.
        tw = typed_rect[0] + untyped_rect[0]
        offset = (self.parent.width - tw) // 2
        
        #Calc the text position based on the offset.
        tx = round(self.parent.x) + offset - camera.x
        ty = round(self.parent.y) - camera.y - typed_rect[1]
        
        #Actually draw the box and text to screen.
        screen.blit(self.box, (tx-5, ty-5))
        screen.blit( self.font.render(self.typed, True, (0,183,0)) , (tx,ty))
        screen.blit( self.font.render(untyped, True, (0,0,0)) , (tx+typed_rect[0],ty))

def load_sprite(file):
    image = pygame.image.load(os.path.join('Sprites', file))
    size = image.get_size()
    return pygame.transform.scale(image, (size[0]*4, size[1]*4))

def choose_word(difficulty):
    if difficulty == "easy":
        min_length = 3
        max_length = 5
    elif difficulty == "normal":
        min_length = 5
        max_length = 8
    elif difficulty == "difficult":
        min_length = 7
        max_length = 13
    elif difficulty == "uber":
        min_length = 9
        max_length = 15
        
    with open('wordlist.txt') as wordbook:
        words = [line for line in wordbook if max_length+1 >= len(line) >= min_length]
    return random.choice(words).rstrip('\n')
    
def generate_captcha(difficulty):
    chars = list(string.ascii_letters)
    chars += list(string.digits)
    chars.remove("I")
    chars.remove("l")
    chars.remove("O")
    chars.remove("0")
    symbols = ['!', '?', '#', '%', '&', '@', '*', '=', "-", "+", "_", ":"]
    word = ""
    
    if difficulty == "easy":
        char_count = 4
        symbol_count = 1
    if difficulty == "normal":
        char_count = 6
        symbol_count = 2
    if difficulty == "difficult":
        char_count = 8
        symbol_count = 3
    
    #Generate 7 random letters (mix of upper and lower case)
    for i in range(char_count):
        word += random.choice(chars)
            
    #Insert 1 or 2 symbols at random
    for i in range(symbol_count):
        symbol_pos = random.randrange(len(word))
        word = word[:symbol_pos] + random.choice(symbols) + word[symbol_pos:]
    
    return word