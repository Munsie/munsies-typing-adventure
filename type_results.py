import pygame
import sys
import os

def calc_letter_grade(stats, accuracy):
    GOLD = (255,215,0)
    SILVER = (192,192,192)
    BRONZE = (205,127,50)
    WHITE = (255,255,255)
    
    colour = WHITE
    grade = "F"

    if stats.bosses_killed == 0:
        colour = WHITE
    elif stats.bosses_killed == 1:
        colour = BRONZE
    elif stats.bosses_killed == 2:
        colour = SILVER
    elif stats.bosses_killed == 3:
        colour = GOLD
    
    if stats.dead == True:
        return(grade, colour)

    if accuracy <= 70:
        grade = "F"
    elif accuracy <= 80:
        grade = "D"
    elif accuracy <= 85:
        grade = "C"
    elif accuracy <= 90:
        grade = "B"
    elif accuracy <= 95:
        grade = "A"
    elif accuracy > 95:
        grade = "S"
    
    if accuracy == 100:
        grade += "++"
    elif accuracy > 98:
        grade += "+"      
    
    return (grade, colour)

def results(screen, clock, bg, stats, main_font, title_font):
    #Calculate final player accuracy
    if stats.total_keys == 0:
        pct = 0
    else:
        pct = (stats.correct_keys/stats.total_keys)*100
    accuracy = "{0:.2f}".format(pct) + "%"
    
    #Calculate final letter grade & colour
    letter_grade, letter_colour = calc_letter_grade(stats, pct)
    
    #Determine final player status
    if stats.dead == True:
        status = "Dead!"
    elif stats.escaped == True:
        status = "Escaped!"
    else:
        status = "Unknown"
        letter_grade = "F"
        letter_colour = (255,255,255)

    #Calculate minutes and seconds
    minutes, seconds = divmod(stats.time_elapsed//1000, 60)
    
    letter_font = pygame.font.Font(os.path.join('Fonts', 'Ubuntu-Regular.ttf'), 156)
    delta_time = 0
    menu_time = 0 #Used to reveal stats one at a time
    
    looping = True

    while looping:
        for event in pygame.event.get():
            if event.type == pygame.QUIT: sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE: looping = False
        
        menu_time += delta_time
        
        #Draw results
        screen.fill((0,0,0))
        
        screen.blit(bg, (-5,0)) #Render the background sprite.
        
        screen.blit( title_font.render("Munsie's Typing Adventure", True, (255,255,255)) , (60,150))
        screen.blit( main_font.render("Statistics", True, (255,255,255)), (80, 300))
        
        #Column 1
        if menu_time >= 1000:
            screen.blit( main_font.render(f"Status: {status}", True, (255,255,255)) , (100,350))
        if menu_time >= 2000:
            screen.blit( main_font.render(f"Difficulty: {stats.difficulty}", True, (255,255,255)) , (100,400))
        if menu_time >= 3000:
            screen.blit( main_font.render(f"Time elapsed: {minutes:02d}:{seconds:02d}", True, (255,255,255)) , (100,450))
        
        #Column 2
        if menu_time >= 4000:
            screen.blit( main_font.render(f"Keys pressed: {stats.total_keys}", True, (255,255,255)) , (450,350))
        if menu_time >= 5000:
            screen.blit( main_font.render(f"Keys correct: {stats.correct_keys}", True, (255,255,255)) , (450,400))
        if menu_time >= 6000:
            screen.blit( main_font.render(f"Key accuracy: {accuracy}", True, (255,255,255)) , (450,450))
        
        #Column 3
        if menu_time >= 7000:
            screen.blit( main_font.render(f"Robots killed: {stats.enemies_killed}", True, (255,255,255)) , (800,350))
        if menu_time >= 8000:
            screen.blit( main_font.render(f"Bosses killed: {stats.bosses_killed}/3", True, (255,255,255)) , (800,400))
        if menu_time >= 9000:
            screen.blit( main_font.render(f"Damage taken: {stats.damage_taken}", True, (255,255,255)) , (800,450))
            
        #Letter grade
        if menu_time >= 10000:
            letter_surf = letter_font.render(letter_grade, True, letter_colour)
            screen.blit(letter_surf, (1165,325))
        
        pygame.display.flip()
        
        delta_time = clock.tick(144)