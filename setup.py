from cx_Freeze import setup,Executable

includefiles = ['Fonts/', 'Sprites/', 'data.json', 'wordlist.txt']
excludemodules = ['numpy', 'asyncio', 'email', 'html', 'http', 'logging', 'curses', 'xmlprc', 'concurrent', 'urllib', 'lib2to3', 'unittest', 'ctypes', 'pycparser', 'cffi', 'multiprocessing', 'pkg_resources', 'pydoc_data', 'socket', '_queue', 'lzma', 'hashlib', 'bz2']

setup(
    name = "Munsie's Typing Adventure",
    version = '1.1',
    description = 'Pew pew spaceship typing game',
    author = 'Jake Munsie',
    options = {'build_exe': {'include_files':includefiles, 'excludes':excludemodules}}, 
    executables = [Executable('play.py')]
)